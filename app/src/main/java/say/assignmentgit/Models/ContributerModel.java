package say.assignmentgit.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Che on 12-11-2017.
 */

public class ContributerModel implements Parcelable {
    String imageUrl;
    String repoUrl;
    String ContriName;

    public ContributerModel(){

    }
    protected ContributerModel(Parcel in) {
        imageUrl = in.readString();
        repoUrl = in.readString();
        ContriName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageUrl);
        dest.writeString(repoUrl);
        dest.writeString(ContriName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContributerModel> CREATOR = new Creator<ContributerModel>() {
        @Override
        public ContributerModel createFromParcel(Parcel in) {
            return new ContributerModel(in);
        }

        @Override
        public ContributerModel[] newArray(int size) {
            return new ContributerModel[size];
        }
    };

    public String getContriName() {
        return ContriName;
    }

    public void setContriName(String contriName) {
        ContriName = contriName;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getRepoUrl() {
        return repoUrl;
    }

    public void setRepoUrl(String repoUrl) {
        this.repoUrl = repoUrl;
    }
}
