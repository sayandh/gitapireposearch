package say.assignmentgit.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Che on 11-11-2017.
 */


public class RepoDetailsModel implements Parcelable {
    String name;
    String fullName;
    String contributersUrl;
    String description;
    int watchersCount;
    int commitCount;
    String imageUrl;
    String pageUrl;

    public RepoDetailsModel(){

    }

    protected RepoDetailsModel(Parcel in) {
        name = in.readString();
        fullName = in.readString();
        contributersUrl = in.readString();
        description = in.readString();
        watchersCount = in.readInt();
        commitCount = in.readInt();
        imageUrl = in.readString();
        pageUrl=in.readString();

    }

    public static final Creator<RepoDetailsModel> CREATOR = new Creator<RepoDetailsModel>() {
        @Override
        public RepoDetailsModel createFromParcel(Parcel in) {
            return new RepoDetailsModel(in);
        }

        @Override
        public RepoDetailsModel[] newArray(int size) {
            return new RepoDetailsModel[size];
        }
    };


    public String getContributersUrl() {
        return contributersUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setContributersUrl(String contributersUrl) {
        this.contributersUrl = contributersUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullName() {
        return fullName;
    }



    public String getName() {
        return name;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public void setName(String name) {
        this.name = name;
    }



    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setCommitCount(int commitCount) {
        this.commitCount = commitCount;
    }

    public int getCommitCount() {
        return commitCount;
    }

    public int getWatchersCount() {
        return watchersCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setWatchersCount(int watchersCount) {
        this.watchersCount = watchersCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(fullName);
        dest.writeString(contributersUrl);
        dest.writeString(description);
        dest.writeInt(watchersCount);
        dest.writeInt(commitCount);
        dest.writeString(imageUrl);
        dest.writeString(pageUrl);
    }
}
