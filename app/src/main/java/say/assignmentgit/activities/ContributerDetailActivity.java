package say.assignmentgit.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import say.assignmentgit.Models.ContributerModel;
import say.assignmentgit.Models.RepoDetailsModel;
import say.assignmentgit.R;
import say.assignmentgit.adapters.RecyclertouchListener;
import say.assignmentgit.adapters.RepoRecyclerViewAdapter;
import say.assignmentgit.others.Utilities;

/**
 * Created by Che on 12-11-2017.
 */

public class ContributerDetailActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    LinearLayout layoutLoading;
    ContributerModel contributerModel;
    RepoRecyclerViewAdapter repoRecyclerViewAdapter;
    ArrayList<String> arrayListRepoName=new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributer);

        setTitle("Contributer Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        contributerModel = getIntent().getExtras().getParcelable("contributerDetails");

        ImageView imageView = (ImageView) findViewById(R.id.contri_image);
        TextView textView = (TextView) findViewById(R.id.contri_name);
        recyclerView = (RecyclerView) findViewById(R.id.cont_repo_list);
        layoutLoading=(LinearLayout)findViewById(R.id.layoutLoadingRopo) ;

        Picasso.with(getApplicationContext()).load(contributerModel.getImageUrl()).into(imageView);
        textView.setText(contributerModel.getContriName());

        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(recylerViewLayoutManager);

        repoRecyclerViewAdapter=new RepoRecyclerViewAdapter(ContributerDetailActivity.this,arrayListRepoName);
        recyclerView.setAdapter(repoRecyclerViewAdapter);

        new RepoTask().execute();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return true;
    }

    class RepoTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String result = "";
            InputStream is = null;
            try {


                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(
                        contributerModel.getRepoUrl());

                HttpResponse response = httpClient.execute(httpGet);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();


            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            final ArrayList<RepoDetailsModel> arrayListRepodetails= Utilities.parseRepoList(s);
            arrayListRepoName=new ArrayList<>();

            for (int n=0;n<arrayListRepodetails.size();n++){
                arrayListRepoName.add(arrayListRepodetails.get(n).getName());
            }

            repoRecyclerViewAdapter=new RepoRecyclerViewAdapter(ContributerDetailActivity.this,arrayListRepoName);
            recyclerView.setAdapter(repoRecyclerViewAdapter);


            repoRecyclerViewAdapter.notifyDataSetChanged();
            recyclerView.setVisibility(View.VISIBLE);
            layoutLoading.setVisibility(View.GONE);


            recyclerView.addOnItemTouchListener(new RecyclertouchListener(getApplicationContext(), new RecyclertouchListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Intent intent=new Intent(ContributerDetailActivity.this,RepoDetailsActivity.class);
                    intent.putExtra("details",arrayListRepodetails.get(position));
                    startActivity(intent);
                }
            }));

        }
    }
}
