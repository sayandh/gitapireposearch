package say.assignmentgit.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import say.assignmentgit.Models.RepoDetailsModel;
import say.assignmentgit.R;
import say.assignmentgit.adapters.RecyclerViewAdapterMain;
import say.assignmentgit.adapters.RecyclertouchListener;
import say.assignmentgit.adapters.RepoRecyclerViewAdapter;
import say.assignmentgit.others.Constants;
import say.assignmentgit.others.Utilities;


public class MainActivity extends AppCompatActivity {

    EditText editTextSearch;
    RecyclerView recyclerView;
    ProgressBar imageButtonLoad;
    RecyclerViewAdapterMain recyclerViewAdapter;
    Button buttonSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main);
        ArrayList<RepoDetailsModel> repoDetailsModelArrayList=new ArrayList<>();

        setTitle("Home");

        editTextSearch=(EditText) findViewById(R.id.search);
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        imageButtonLoad=(ProgressBar)findViewById(R.id.image_button);
        buttonSearch=(Button)findViewById(R.id.button_search);

        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(recylerViewLayoutManager);

        recyclerViewAdapter=new RecyclerViewAdapterMain(MainActivity.this,repoDetailsModelArrayList);
        recyclerView.setAdapter(recyclerViewAdapter);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyword=editTextSearch.getEditableText().toString().trim();
                new searchTask().execute(keyword);
            }
        });


    }

    private class searchTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            imageButtonLoad.setVisibility(View.VISIBLE);
            buttonSearch.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... params) {

            final String url= Constants.GIT_URL+params[0]+Constants.SORT_URL;

            String result = "";
            InputStream is = null;
            try {


                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(
                        url);

                HttpResponse response = httpClient.execute(httpGet);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();


            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            imageButtonLoad.setVisibility(View.GONE);
            buttonSearch.setVisibility(View.VISIBLE);
            final ArrayList<RepoDetailsModel> repoDetailsModelArrayList=Utilities.parseJson(s);

            recyclerViewAdapter=new RecyclerViewAdapterMain(MainActivity.this,repoDetailsModelArrayList);
            recyclerView.setAdapter(recyclerViewAdapter);

            recyclerViewAdapter.notifyDataSetChanged();

            recyclerView.addOnItemTouchListener(new RecyclertouchListener(getApplicationContext(), new RecyclertouchListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Intent intent=new Intent(MainActivity.this,RepoDetailsActivity.class);
                    intent.putExtra("details",repoDetailsModelArrayList.get(position));

                    startActivity(intent);
                }
            }));


        }
}
}
