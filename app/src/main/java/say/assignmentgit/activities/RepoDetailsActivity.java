package say.assignmentgit.activities;

import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import say.assignmentgit.Models.ContributerModel;
import say.assignmentgit.Models.RepoDetailsModel;
import say.assignmentgit.R;
import say.assignmentgit.adapters.ContributersAdapter;

/**
 * Created by Che on 11-11-2017.
 */

public class RepoDetailsActivity extends AppCompatActivity {
    RepoDetailsModel repoDetailsModel;
    TextView textViewName, textViewDescription,textViewLoading,textViewLink;
    ImageView imageView;
    GridView gridView;
    LinearLayout linearLayoutLoading;
    ProgressBar imageButtonLoading;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        setTitle("Repo Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewName = (TextView) findViewById(R.id.text_name_repo);
        ExpandableTextView expTv1 = (ExpandableTextView)findViewById(R.id.expand_text_view);
        imageView = (ImageView) findViewById(R.id.image_view_repo);
        gridView=(GridView)findViewById(R.id.grid_view);
        linearLayoutLoading=(LinearLayout)findViewById(R.id.layoutLoading);
        textViewLoading=(TextView)findViewById(R.id.text_loading);
        imageButtonLoading=(ProgressBar)findViewById(R.id.image_button_loading);
        textViewLink=(TextView)findViewById(R.id.text_link_repo);


        repoDetailsModel = getIntent().getExtras().getParcelable("details");

        textViewName.setText("Name : " + repoDetailsModel.getName());

        expTv1.setText(repoDetailsModel.getDescription());
        Picasso.with(getApplicationContext()).load(repoDetailsModel.getImageUrl()).into(imageView);

        new ContributerTask().execute();


        textViewLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RepoDetailsActivity.this,WebviewActivity.class);
                intent.putExtra("link",repoDetailsModel.getPageUrl());
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return true;
    }


    class ContributerTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            String result = "";
            InputStream is = null;
            try {


                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(
                        repoDetailsModel.getContributersUrl());

                HttpResponse response = httpClient.execute(httpGet);

                    HttpEntity entity = response.getEntity();

                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();

            }catch (NullPointerException e){
                e.printStackTrace();
            }catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            final ArrayList<ContributerModel> contributerModelArrayList=new ArrayList<>();
            try {
                JSONArray jsonArray=new JSONArray(s);
                for (int n=0;n<jsonArray.length();n++){
                    JSONObject jsonObject=jsonArray.getJSONObject(n);
                    ContributerModel contributerModel=new ContributerModel();

                    contributerModel.setImageUrl(jsonObject.getString("avatar_url"));
                    contributerModel.setRepoUrl(jsonObject.getString("repos_url"));
                    contributerModel.setContriName(jsonObject.getString("login"));
                    contributerModelArrayList.add(contributerModel);
                }

                ContributersAdapter contributersAdapter=new ContributersAdapter(getApplicationContext(),contributerModelArrayList);
                gridView.setAdapter(contributersAdapter);
                gridView.setVisibility(View.VISIBLE);
                linearLayoutLoading.setVisibility(View.GONE);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent=new Intent(RepoDetailsActivity.this,ContributerDetailActivity.class);
                        intent.putExtra("contributerDetails",contributerModelArrayList.get(position));
                        startActivity(intent);
                    }
                });

            }catch (JSONException e){
                e.printStackTrace();
                imageButtonLoading.setVisibility(View.GONE);
                textViewLoading.setText("Sorry No Contributers Found");
                textViewLoading.setTextColor(Color.parseColor("#F44336"));
            }
        }
    }
}
