package say.assignmentgit.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import say.assignmentgit.R;

/**
 * Created by Che on 12-11-2017.
 */

public class WebviewActivity extends AppCompatActivity {

    private WebView myWebView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String link=getIntent().getStringExtra("link");
        setTitle("Git Page");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_webview);
        myWebView = (WebView)findViewById(R.id.webView);
        progressBar=(ProgressBar)findViewById(R.id.progreebar);

        progressBar.setVisibility(View.VISIBLE);
        myWebView.setVisibility(View.GONE);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        myWebView.clearCache(true);
        myWebView.clearHistory();
        myWebView.setWebViewClient(new Webview());
        myWebView.loadUrl(link);
        myWebView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                myWebView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);


            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()){
          case android.R.id.home:
              finish();
              return true;
      }
      return true;
    }

    @Override
    public void onBackPressed() {
        if(myWebView.canGoBack()) {
            myWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }



    private class Webview extends WebViewClient{

        public void onPageFinished(WebView view, String url) {

            // show webview
//            findViewById(R.id.webView).setVisibility(View.VISIBLE);
            // hide splash screen





        }
    }
}