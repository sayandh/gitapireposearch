package say.assignmentgit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import say.assignmentgit.Models.ContributerModel;
import say.assignmentgit.R;

/**
 * Created by Che on 11-11-2017.
 */

public class ContributersAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ContributerModel> contributerModelArrayList;


    // Constructor
    public ContributersAdapter(Context c,ArrayList<ContributerModel>contributerModelArrayList){
        mContext = c;
        this.contributerModelArrayList=contributerModelArrayList;
    }

    @Override
    public int getCount() {
        return contributerModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return contributerModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ContributerModel rowItem=contributerModelArrayList.get(position);
        View view;
        if(convertView==null) {

            LayoutInflater inflater =(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_contributer, parent, false);
        }
        view=convertView;
        TextView textView=(TextView)view.findViewById(R.id.text_contribuetr_name);
        textView.setText(rowItem.getContriName());
        ImageView imageView=(ImageView)view.findViewById(R.id.image_contributer);
        Picasso.with(mContext).load(rowItem.getImageUrl()).into(imageView);
        return convertView;
    }

}
