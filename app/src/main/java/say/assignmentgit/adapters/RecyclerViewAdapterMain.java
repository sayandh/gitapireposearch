package say.assignmentgit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import say.assignmentgit.Models.RepoDetailsModel;
import say.assignmentgit.R;

/**
 * Created by Che on 11-11-2017.
 */

public class RecyclerViewAdapterMain extends RecyclerView.Adapter<RecyclerViewAdapterMain.ViewHolder>{

    int displaySize=10;
    ArrayList<RepoDetailsModel>repoDetailsModelArrayList;
    Context context;
    View view;
    ViewHolder viewHolder1;


    public RecyclerViewAdapterMain(Context context1, ArrayList<RepoDetailsModel> repoDetailsModelArrayList){

        this.repoDetailsModelArrayList=repoDetailsModelArrayList;
        this.context = context1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nameTxt,fullNameTxt,watcherCountTxt;
        ImageView imageView;


        private ViewHolder(View v){

            super(v);
            nameTxt=(TextView)v.findViewById(R.id.text_name);
            fullNameTxt=(TextView)v.findViewById(R.id.text_full_name);
            watcherCountTxt=(TextView)v.findViewById(R.id.text_watcher_count);
            imageView=(ImageView)v.findViewById(R.id.image_view_avatar);
        }
    }

    @Override
    public RecyclerViewAdapterMain.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType){

        view = LayoutInflater.from(context).inflate(R.layout.list_items_search,parent,false);


        viewHolder1 = new ViewHolder(view);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){

        RepoDetailsModel repoDetailsModel=repoDetailsModelArrayList.get(position);

        holder.nameTxt.setText("Name : "+repoDetailsModel.getName());
        holder.fullNameTxt.setText("FullName : "+repoDetailsModel.getFullName());
        holder.watcherCountTxt.setText("Watchers : "+String.valueOf(repoDetailsModel.getWatchersCount()));
        Picasso.with(context).load(repoDetailsModel.getImageUrl()).into(holder.imageView);

    }

    @Override
    public int getItemCount(){
        if(displaySize > repoDetailsModelArrayList.size())
            return repoDetailsModelArrayList.size();
        else
            return displaySize;

    }

}