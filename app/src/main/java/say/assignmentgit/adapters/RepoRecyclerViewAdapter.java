package say.assignmentgit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import say.assignmentgit.R;

/**
 * Created by Che on 12-11-2017.
 */

public class RepoRecyclerViewAdapter extends RecyclerView.Adapter<RepoRecyclerViewAdapter.ViewHolder>{

    ArrayList<String> repoDetailsModelArrayList;
    Context context;
    View view;
    RepoRecyclerViewAdapter.ViewHolder viewHolder1;


    public RepoRecyclerViewAdapter(Context context1,ArrayList<String> repoDetailsModelArrayList){

        this.repoDetailsModelArrayList=repoDetailsModelArrayList;
        this.context = context1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView numberText,repoNameTxt;


        public ViewHolder(View v){

            super(v);
            repoNameTxt=(TextView)v.findViewById(R.id.repo_name);

           numberText=(TextView)v.findViewById(R.id.repo_no);
        }
    }

    @Override
    public RepoRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType){

        view = LayoutInflater.from(context).inflate(R.layout.list_item_repo,parent,false);


        viewHolder1 = new RepoRecyclerViewAdapter.ViewHolder(view);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(RepoRecyclerViewAdapter.ViewHolder holder1, int position){

        holder1.repoNameTxt.setText(repoDetailsModelArrayList.get(position));

            holder1.numberText.setText(position+1+" .");




    }

    @Override
    public int getItemCount(){
            return repoDetailsModelArrayList.size();

    }

}
