package say.assignmentgit.others;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import say.assignmentgit.Models.RepoDetailsModel;

/**
 * Created by Che on 11-11-2017.
 */

public class Utilities {

    public static ArrayList<RepoDetailsModel> parseJson(String responce){
        ArrayList<RepoDetailsModel> repoDetailsModelArrayList=new ArrayList<>();
        try {
            JSONObject jsonObject=new JSONObject(responce);
            JSONArray jsonArrayItems=jsonObject.getJSONArray("items");

            for (int n=0;n<jsonArrayItems.length();n++){

                JSONObject itemDetails=jsonArrayItems.getJSONObject(n);

                RepoDetailsModel repoDetailsModel=new RepoDetailsModel();

                repoDetailsModel.setName(itemDetails.getString("name"));
                repoDetailsModel.setFullName(itemDetails.getString("full_name"));

                repoDetailsModel.setWatchersCount(itemDetails.getInt("watchers_count"));
                repoDetailsModel.setContributersUrl(itemDetails.getString("contributors_url"));
                repoDetailsModel.setDescription(itemDetails.getString("description"));
                repoDetailsModel.setPageUrl(itemDetails.getString("html_url"));


                JSONObject jsonObjectOwner=itemDetails.getJSONObject("owner");
                repoDetailsModel.setImageUrl(jsonObjectOwner.getString("avatar_url"));


                repoDetailsModelArrayList.add(repoDetailsModel);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }




        Collections.sort(repoDetailsModelArrayList, new Comparator<RepoDetailsModel>() {
            @Override
            public int compare(RepoDetailsModel o1, RepoDetailsModel o2) {
                return Double.valueOf(o2.getWatchersCount()).compareTo(Double.valueOf(o1.getWatchersCount()));
            }
        });
        return repoDetailsModelArrayList;
    }

    public static ArrayList<RepoDetailsModel> parseRepoList(String responce){
        ArrayList<RepoDetailsModel> repoDetailsModelArrayList=new ArrayList<>();
        try {
            JSONArray jsonArrayItems=new JSONArray(responce);

            for (int n=0;n<jsonArrayItems.length();n++){

                JSONObject itemDetails=jsonArrayItems.getJSONObject(n);

                RepoDetailsModel repoDetailsModel=new RepoDetailsModel();

                repoDetailsModel.setName(itemDetails.getString("name"));
                repoDetailsModel.setFullName(itemDetails.getString("full_name"));

                repoDetailsModel.setWatchersCount(itemDetails.getInt("watchers_count"));
                repoDetailsModel.setContributersUrl(itemDetails.getString("contributors_url"));
                repoDetailsModel.setDescription(itemDetails.getString("description"));
                repoDetailsModel.setPageUrl(itemDetails.getString("html_url"));

                JSONObject jsonObjectOwner=itemDetails.getJSONObject("owner");
                repoDetailsModel.setImageUrl(jsonObjectOwner.getString("avatar_url"));


                repoDetailsModelArrayList.add(repoDetailsModel);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }




        Collections.sort(repoDetailsModelArrayList, new Comparator<RepoDetailsModel>() {
            @Override
            public int compare(RepoDetailsModel o1, RepoDetailsModel o2) {
                return Double.valueOf(o2.getWatchersCount()).compareTo(Double.valueOf(o1.getWatchersCount()));
            }
        });
        return repoDetailsModelArrayList;
    }



}
